export default {
  loginExpiredCode: [401,403], // 用户信息过期的code
  origin: process.env.NODE_ENV === 'development' ? 'https://friendlyapi.yuelinshe.com' : 'https://friendlyapi.yuelinshe.com', // 配置请求的域名
  origin1: process.env.NODE_ENV === 'development' ? 'https://friendlyapi.yuelinshe.com' : 'https://friendlyapi.yuelinshe.com' // 用于设置多个域名
}