import Vue from 'vue'
import App from './App'
import store from './store'
import {request, upload} from './utils/request.js'
import * as api from './api'
import GoEasy from "./sdk/goeasy-2.0.13.min.js";

Vue.config.productionTip = false

Vue.prototype.$store = store
Vue.prototype.$request = request
Vue.prototype.$upload = upload
Vue.prototype.$api = api

Vue.prototype.$setToken = (token,hasAddBearer)=>{
	const tokenStr = hasAddBearer? token : 'Bearer ' + token
	store.commit('setUserToken',tokenStr)
	uni.setStorageSync('token', tokenStr);
}

Vue.prototype.$getGlobalDataWhenLogin = ()=>{
	api.getUserInfo().then((res) => {
		store.commit('setUserInfo',res)
	})
	
	api.getDictType('evaluate_icon').then((res)=>{
		store.commit('setEvaluateIcon',res)
	})
	
	api.getDictType('evaluate_grade').then((res)=>{
		store.commit('setEvaluateGrade',res)
	})
	
	api.getDictType('identity_desc').then((res)=>{
		store.commit('setIdentityDesc',res)
	})
	api.getDictType('user_label').then((res)=>{
		store.commit('setUserLabel',res)
	})
	api.getDictType('shareImages').then((res)=>{
		store.commit('setShareImages',res)
	})
	
	store.dispatch('getFriendList')
}


const goEasy = GoEasy.getInstance({
    host:'hangzhou.goeasy.io',
    appkey: 'BC-fd1c3f7cf812429299ac354541e37d91',
    // true表示支持通知栏提醒，false则表示不需要通知栏提醒
    allowNotification:true, //仅有效于app,小程序和H5将会被自动忽略
    modules:["im"]
});

Vue.prototype.GoEasy = GoEasy;
Vue.prototype.goEasy = goEasy;

Vue.prototype.formatDate = function (t) {
    t = t || Date.now();
    let time = new Date(t);
    let str = time.getMonth() < 9 ? ('0' + (time.getMonth() + 1)) : (time.getMonth() + 1);
    str += '-';
    str += time.getDate() < 10 ? ('0' + time.getDate()) : time.getDate();
    str += ' ';
    str += time.getHours();
    str += ':';
    str += time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes();
    return str;
}


Vue.mixin({
	methods:{
		// 根据图片id获得图片
		getImgUrl(id,type){
			id = id + ''
			if(id.indexOf('http') !== -1){
				return id
			}
			if(/[^0-9]+/g.test(id) || !id){
				if(type === 'emoji'){
					id = '179542906417221'
				}else{
					id = '179533026287685'
				}
			}
			return this.$store.state.globalConfig.origin + '/api/settingservice/previewfileinfo?id=' + id
		}
	}
})

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
