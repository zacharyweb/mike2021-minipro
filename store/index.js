import Vue from 'vue'
import Vuex from 'vuex'
import config from '../config'
import {
	request
} from './../utils/request.js'
import * as api from '../api'
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		token: '',
		userWxProfile: {},
		userWxOpenidData: {},
		userInfo: {},
		globalConfig: {
			...config
		},
		evaluateIcon: [],
		evaluateGrade: [],
		identityDesc: [],
		userLabels:[],
		shareImages:[],
		selectedUserLabels:[],
		newEvaluate: {
			id:'',
			identityDesc:'',
			evaluateGrade: '',
			evaluateIcon: '',
			evaluateDesc: '',
			evaluateIconId:'',
			toUserInfo:{},
			toSeeType:1, // 1.公开 2.部分可见 3.部分不可见
			toSeeUsers:[],
			toNotSeeUsers:[]
		},
		friendList:[],
		fromUserId:'',
		fromVirtualId:'',
		tempIdentityDesc:'', // 暂存用来记录专业
		unReadMsgNum: 0,//未读消息数量
		unReadChatNum: 0
	},
	getters: {},
	mutations: {
		setToken(state, val) {
			state.token = val
		},
		setUserWxProfile(state, val) {
			state.userWxProfile = val
		},
		setUserWxOpenidData(state, val) {
			state.userWxOpenidData = val
		},
		setUserToken(state, val) {
			state.token = val
		},
		setUserInfo(state, val) {
			state.userInfo = val
		},
		setEvaluateIcon(state, val) {
			state.evaluateIcon = val
		},
		setEvaluateGrade(state, val) {
			state.evaluateGrade = val
		},
		setIdentityDesc(state, val) {
			state.identityDesc = val
		},
		setUserLabel(state, val){
			state.userLabels = val
		},
		setShareImages(state, val){
			state.shareImages = val
		},
		setSelectedUserLabels(state, val){
			state.selectedUserLabels = val
		},

		updateNewEvaluate(state, data) {
			state.newEvaluate = {
				...state.newEvaluate,
				...data
			}
		},

		resetNewEvaluate(state) {
			state.newEvaluate = {
				id:'',
				identityDesc:'',
				evaluateGrade: '',
				evaluateIcon: '',
				evaluateDesc: '',
				evaluateIconId:'',
				toUserInfo:{},
				toSeeType:1, // 1.公开 2.部分可见 3.部分不可见
				toSeeUsers:[],
				toNotSeeUsers:[]
			}
		},
		
		setFriendList(state,data){
			state.friendList = data
		},
	
		setFromUserId(state,id){
			state.fromUserId = id
		},
		setFromVirtualId(state,id){
			state.fromVirtualId = id
		},

		setTempIdentityDesc(state,desc){
			state.tempIdentityDesc = desc
		},
		
		setUnReadMsgNum(state,num){
		   state.unReadMsgNum = num
		},
		
		setUnReadChatNum(state,num){
			state.unReadChatNum = num
		}
	},
	
	actions: {
		getFriendList({commit},data={}){
			api.getFriendList(data,{ notJumpToLogin:false , notShowErrorTips:false}).then((res)=>{
				commit('setFriendList',res)
			})
		},
		getNoReadMsg({commit}){
			api.getNoReadMsg().then((res)=>{
				commit('setUnReadMsgNum',res)
			})
		}
	}
})

export default store
