import { request } from '../utils/request.js'


// 获取朋友列表
function getFriendList(data,config){
	return request({
	  url: '/api/friendshipservice/getfriendlist',
	  origin: 1,
	  method: 'GET',
	  data: {
		 ...data
	  },
	  notJumpToLogin: config.notJumpToLogin,
	  notShowErrorTips: config.notShowErrorTips
	})
}

// 发送好友申请
function sendAddFriendApply(data){
	return request({
	  url: '/api/friendshipservice/friendshipadd',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  },
	  notShowErrorTips: true
	})
}

// 通过好友申请
function passAddFriendApply(id){
	return request({
	  url: '/api/friendshipservice/passforadd/' + id,
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 拒绝好友申请
function denyAddFriendApply(id){
	return request({
	  url: '/api/friendshipservice/denyforadd/' + id,
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 获取好友申请列表
function getFriendApplyList(data){
	return request({
	  url: '/api/friendshipservice/getcurrentuserfriendshipmsglist',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}

// 获取好友申请详情
function getFriendApplyDetail(id){
	return request({
	  url: '/api/friendshipservice/getcurrentuserfriendshipmsgdetail/' + id,
	  origin: 1,
	  method: 'GET',
	  data: {}
	})
}

// 通过点评发起的好友申请
function sendAddFriendApplyByEvaluate(data){
	return request({
	  url: '/api/commentservice/shareevaluatetoaddfriendship',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}

// 同意介绍被点评人给好友
function agreeFriendApplyByEvaluate(data){
	return request({
	  url: '/api/commentservice/isagreeaddfriendshipbyshareevaluate',
	  origin: 1,
	  method: 'POST',
	  data: {
		 ...data
	  }
	})
}

// 获取一个全局唯一ID
function getVirtualId(data = {}){
	return request({
		url: '/api/settingservice/getvirtualid',
		origin: 1,
		method: 'GET',
		data: {
		   ...data
		},
		notShowErrorTips: true
	})
}

// 绑定虚拟ID和用户之间的关系
function setVirtualIdToUser(data){
	return request({
		url: '/api/settingservice/setvirtualidanduser',
		origin: 1,
		method: 'POST',
		data: {
		   ...data
		},
		notShowErrorTips: true
	})
}

// 绑定虚拟ID的点评和用户的关系
function setVirtualCommentIdToUser(data){
	return request({
		url: '/api/settingservice/updateevaluatetouserid',
		origin: 1,
		method: 'POST',
		data: {
		   ...data
		},
		notShowErrorTips: true
	})
}

export {
   getFriendList,
   sendAddFriendApply,
   passAddFriendApply,
   denyAddFriendApply,
   getFriendApplyList,
   getFriendApplyDetail,
   sendAddFriendApplyByEvaluate,
   agreeFriendApplyByEvaluate,
   getVirtualId,
   setVirtualIdToUser,
   setVirtualCommentIdToUser
}