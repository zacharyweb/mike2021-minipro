import { request } from '../utils/request.js'
// 获取用户信息
function getUserInfo(){
	return request({
	  url: '/api/settingservice/getuserinfo',
	  origin: 1,
	  method: 'GET',
	  data: {}
	})
}

// 获取用户详细信息
function getUserInfoById(id){
	return request({
	  url: '/api/settingservice/getuserinfo/' + id,
	  origin: 1,
	  method: 'GET',
	  data: {},
	  notShowErrorTips: true
	})
}

function getUserInfoByPhone(phone){
	return request({
	  url: '/api/settingservice/getuserinfobyphone/' + phone,
	  origin: 1,
	  method: 'GET',
	  data: {},
	  notShowErrorTips: true
	})
}


// 更新用户头像图片
function updateAvatar(id){
	return request({
	  url: '/api/settingservice/updateavatar',
	  origin: 1,
	  method: 'PUT',
	  data: {
		 avatar:id + ''
	  }
	})
}

// 更新用户信息
function updateUserInfo(data){
	return request({
	  url: '/api/settingservice/updateuserinfo',
	  origin: 1,
	  method: 'PUT',
	  data: {
		...data
	  }
	})
}

// evaluate_grade 评价等级 
// evaluate_icon 评价表情 
// identity_desc 专业身份
function getDictType(code){
	return request({
	  url: '/api/settingservice/getdicttypedropdown',
	  origin: 1,
	  method: 'GET',
	  data: {
		  code
	  }
	})
}



// 关注
function followUser(id){
	return request({
	  url: '/api/settingservice/follow/' + id,
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 取消关注
function unfollowUser(id){
	return request({
	  url: '/api/settingservice/unfollow/' + id,
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 不让他看
function setUnseeFrineds(data){
	return request({
	  url: '/api/settingservice/donotlethimsee',
	  origin: 1,
	  method: 'POST',
	  data
	})
}

function getUnseeFrineds(){
	return request({
	  url: '/api/settingservice/donotlethimseelist',
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 不看他
function setUnlikeFrineds(data){
	return request({
	  url: '/api/settingservice/donotlookathim',
	  origin: 1,
	  method: 'POST',
	  data
	})
}

function getUnlikeFrineds(){
	return request({
	  url: '/api/settingservice/donotlookathimlist',
	  origin: 1,
	  method: 'POST',
	  data: {}
	})
}

// 发短信消息给未注册的用户
function sendMessageToNewUser(data){
	return request({
	  url: '/api/commentservice/sendsmstoinvitation',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}


export {
   getUserInfo,
   getUserInfoById,
   getUserInfoByPhone,
   updateAvatar,
   updateUserInfo,
   getDictType,
   followUser,
   unfollowUser,
   setUnseeFrineds,
   getUnseeFrineds,
   setUnlikeFrineds,
   getUnlikeFrineds,
   sendMessageToNewUser
}