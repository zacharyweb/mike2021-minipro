import { request } from '../utils/request.js'

// 获取消息列表
function getMsgList(data){
	return request({
	  url: '/api/msglistservice/getallmsglist',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}


// 获取消息详情
function getMsgDetail(id){
	return request({
	  url: '/api/msglistservice/getmsgdetail/' + id,
	  origin: 1,
	  method: 'GET',
	  data: {}
	})
}


// 设置消息已读
function setMsgRead(id){
	return request({
	  url: '/api/msglistservice/isreaded/' + id,
	  origin: 1,
	  method: 'GET',
	  data: {},
	  notShowErrorTips: true
	})
}

// 是否同意分享
function isAgreeShare(data){
	return request({
	  url: '/api/commentservice/isagreeshare',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}

// 获取未读消息数量
function getNoReadMsg(){
	return request({
		url: '/api/msglistservice/getcurrentuserunreadmsglist',
		origin: 1,
		method: 'GET',
		data: {},
		notShowErrorTips: true
	})
}

export {
   getMsgList,
   getMsgDetail,
   setMsgRead,
   isAgreeShare,
   getNoReadMsg
}