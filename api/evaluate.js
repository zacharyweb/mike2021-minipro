import { request } from '../utils/request.js'


// 获取首页评论列表
function getEvaluatelist(data){
    return request({
      url: '/api/commentservice/evaluatelist',
      origin: 1,
      method: 'POST',
	    notJumpToLogin:true,
	    notShowErrorTips:true,
      data: {
		  ...data
	  }
    })
}

// 获取首页不需要登录的评论列表 
function getCommonEvaluatelist(data){
    return request({
      url: '/api/commentservice/evaluatelisttoshow',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}


// 获取评论列表通过id
function getEvaluatelistById(data){
    return request({
      url: '/api/commentservice/evaluatelistforuser',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}


// 获取详情
function getEvaluateDetail(id){
    return request({
      url: '/api/commentservice/evaluatelistdetail/' + id,
      origin: 1,
      method: 'GET',
      data: {}
    })
}


// 添加评论
function addEvaluate(data){
    return request({
      url: '/api/commentservice/evaluateadd',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 点赞
function giveLike(data){
    return request({
      url: '/api/commentservice/givelikes',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 评论
function giveComment(data){
    return request({
      url: '/api/commentservice/givecomment',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 分享评论
function shareComment(data){
    return request({
      url: '/api/commentservice/shareevaluate',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}



export {
   getEvaluatelist,
   getCommonEvaluatelist,
   getEvaluatelistById,
   getEvaluateDetail,
   addEvaluate,
   giveLike,
   giveComment,
   shareComment
}