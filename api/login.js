import { request } from '../utils/request.js'

// 登录
function login(data){
    return request({
      url: '/api/loginservice/signin',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 验证码登录
function loginByCode(data){
    return request({
      url: '/api/loginservice/signinbycode',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 校验手机号是否注册过
function checkPhoneIsreged(phone){
	return request({
	  url: '/api/loginservice/isreged/'+phone,
	  origin: 1,
	  method: 'GET',
	  data: {}
	})
}

// 手机号注册
function registerByPhone(data){
	return request({
	  url: '/api/loginservice/regbyphone',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}


// 根据wx.login的code获取openid
function getOpenid(data){
	return request({
	  url: '/api/loginservice/getwechatuserinfo',
	  origin: 1,
	  method: 'GET',
	  data: {
		  ...data
	  }
	})
}


// 微信登录
function loginByWx(data){
	return request({
	  url: '/api/loginservice/signinbyweixin',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}

// 微信登录绑定手机，直接注册了
function registerByWx(data){
	return request({
	  url: '/api/loginservice/bindingphone',
	  origin: 1,
	  method: 'POST',
	  data: {
		  ...data
	  }
	})
}


// 更改密码
function updatePwd(data){
    return request({
      url: '/api/loginservice/forgetuserpwd',
      origin: 1,
      method: 'POST',
      data: {
		  ...data
	  }
    })
}

// 发送验证码
function sendVcode(data){
    return request({
      url: '/api/smscodeservice/sendsmscode',
      origin: 1, 
      method: 'POST',
      data: {
		  ...data
	  }
    })
}


// 退出登录
function signout(){
    return request({
      url: '/api/loginservice/signout',
      origin: 1, 
      method: 'GET',
      data: {
	  }
    })
}


export {
    login,
	loginByCode,
	checkPhoneIsreged,
	registerByPhone,
	getOpenid,
	loginByWx,
	registerByWx,
	sendVcode,
	updatePwd,
	signout
}